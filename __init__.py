from opsdroid.matchers import match_rasanlu
import logging
import xml.etree.ElementTree as ET
import requests
import json

def setup(opsdroid):
    logging.debug("Loaded level module")

@match_rasanlu('level')
async def main(opsdroid, config, message):
  print(json.dumps(message.rasanlu))
  # Gather XML Data
  link = "http://water.weather.gov/ahps2/hydrograph_to_xml.php?gage=rmdv2&output=xml"
  xmlString = requests.get(link).text

  # Parse XML Data
  root = ET.fromstring(xmlString)

  # Parse Latest Reading
  latest = root.find("./observed/datum")


  # Print latest reading
  text = "River Level is: %s" % latest.find("primary").text
  logging.debug(text)
  await message.respond(text)
  await message.respond("http://water.weather.gov/resources/hydrographs/rmdv2_hg.png")



if __name__ == "__main__":
    """ This is executed when run from the command line """
    main()
